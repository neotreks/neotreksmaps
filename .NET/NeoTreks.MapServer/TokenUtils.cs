﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace NeoTreks.MapServer
{
    public class TokenUtils
    {
        private static TokenUtils instance;
        private DateTime tokenExpirationTime;
        private String userName;
        private String password;
        private String tokenURL;
        private int tokenLifetimeHours;
        private String referer;
        private String token;

        private TokenUtils()
        {
            userName = ReadSetting("NeoTreks.UserName");
            password = ReadSetting("NeoTreks.Password");
            tokenURL = ReadSetting("NeoTreks.TokenURL");
            tokenLifetimeHours = Int32.Parse(ReadSetting("NeoTreks.TokenLifetime.Hours"));
            referer = HttpContext.Current.Request.Url.GetComponents(UriComponents.HostAndPort, UriFormat.Unescaped);
        }

        static String ReadSetting(string key)
        {            
            var appSettings = ConfigurationManager.AppSettings;
            string result = appSettings[key] ?? "Not Found";
            return result;
        }

        public static TokenUtils getInstance()
        {
            if (instance == null)
            {
                instance = new TokenUtils();
            }
            return instance;
        }

        public String GetToken()
        {
            if (token != null && tokenExpirationTime > DateTime.UtcNow)
            {
                return token;
            }
            using (WebClient client = new WebClient())
            {
                byte[] response =
                client.UploadValues(tokenURL, new NameValueCollection()
                {
                   { "request", "getToken" },
                   { "username", userName },
                   { "password", password },
                   { "expiration", String.Format("{0}", tokenLifetimeHours * 60) },
                   { "client", "referer" },
                   { "referer", referer},                   
                });

                token = System.Text.Encoding.UTF8.GetString(response);
                tokenExpirationTime = DateTime.UtcNow.AddHours(tokenLifetimeHours);
                return token;
            }
        }
    }
}