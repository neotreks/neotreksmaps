# README #

This are sample .NET and PHP applications for secured NeoTreks Maps.

### What is this repository for? ###

NeoTreks Maps are secured with ArcGIS tokens. By default the ESRI ArcGIS Javascript API is asking user for username and password when the map view loads secured map layer. If you can't share username and password with end users of your site, then you must generate the ArcGIS token manually and hardcode it into your site. This is not secure, because such token must have usually long life and it could be used by someone else. For this reason NeoTreks created .NET and PHP sample application to show how to generate short life tokens in the server behind.

### How do I get set up? ###

* For the .NET sample edit the file .NET/NeoTreks.MapServer/Web.config and fill your NeoTreks Maps username and password
* For the PHP sample edit the file  PHP/index.php and fill your NeoTreks Maps username and password (first top lines of the file)