<?php

error_reporting(0);

//we will need session to remember token, otherwise we would have to generate new token every time
session_start();

class TokenUtils {
    
    private $mTokenURL;
    private $mUsername;
    private $mPassword;
    private $mSessionKey;
    private $mTokenLifeTimeMinutes;

    function __construct($tokenURL, $username, $password, $tokenLifeTimeMinutes) {
        $this->mUsername = $username;
        $this->mPassword = $password;
        $this->mTokenURL = $tokenURL;        
        $this->mTokenLifeTimeMinutes = $tokenLifeTimeMinutes;
        //we will remember generated token in this session key
        $this->mSessionKey = md5($tokenURL . $username);
        $this->getToken();
    }

    public function getToken() {
        $tokenInfo = $_SESSION[$this->mSessionKey];
        if ($tokenInfo === null) {
            //echo "Token is not set<br/>";
            $this->refreshToken();
        }
        else if ($tokenInfo["e"] < time()) {
            //echo "Token has been expired<br/>";
            $this->refreshToken();
        }        
        return $tokenInfo["t"];
    }

    private function refreshToken() {
        //echo "refreshing token<br/>";
        $tokenInfo = array();
        $url = sprintf(
            "%s?request=getToken&username=%s&password=%s&expiration=%d&client=referer&referer=%s",
            $this->mTokenURL,
            urlencode($this->mUsername),
            urlencode($this->mPassword),
            $this->mTokenLifeTimeMinutes,
            $_SERVER["SERVER_NAME"]
        );
        //echo $url."<br/>";
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        ); 
        $token = file_get_contents($url, false, stream_context_create($arrContextOptions));
        $tokenInfo["t"] = $token;
        $tokenInfo["e"] = time() + ($this->mTokenLifeTimeMinutes * 60);
        $_SESSION[$this->mSessionKey] = $tokenInfo;
        return $tokenInfo;
    }
}
?>