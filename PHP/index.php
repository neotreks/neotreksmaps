<?php 
	include("token_utils.php"); 
	$username = "[your username]";
	$password = "[your password]";
	$tokenUtils = new TokenUtils("https://mapserver.neotreks.com/arcgis/tokens/generateToken",$username,$password,60);
	$tokenNeoTreks = $tokenUtils->getToken();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no"/>
	<title>NeoTreks Maps - Recreation and Trail Maps for the Untied States</title>
	<meta name="description=" content="NeoTreks Maps is a recreation and rural-centric map designed to pick up where other major map provider leave off; the great outdoors. NeoTreks Maps boats over a quarter million miles of trails and one of the most comprehensive public land boundary maps available.">
	<link rel="stylesheet" href="https://js.arcgis.com/3.15/dijit/themes/claro/claro.css">    
  <link rel="stylesheet" href="https://js.arcgis.com/3.15/esri/css/esri.css">

	<style type="text/css">
      html, body {
				height: 100%; width: 100%; margin: 0;}
      body{
				font-family: "Trebuchet MS";}
			.dijitTabInnerDiv{
				background-color:#000000;}
	  	a:link, a:visited {
				border-bottom: 0px solid #000000;
				text-decoration: none;
				color: #000000;
				font-size:8pt;}	
	  	a:hover, a:active {
				border-bottom: 0px solid #000000;
				text-decoration: none;
				color: #000000;
				font-size:8pt;}
	  	.esriScalebarLabel{
				color: #000000 !important;
				font-size:8pt !important;}
	  	POIlabel{
				display: block;
				padding-left: 15px;
				text-indent: -1px;}
	  	#leftPane{
				font-size:10pt;
				color:#000000;
				border-top-style:solid;
				border-width:1px;
				box-shadow:0px 0px 4px 4px #303030; 
				background-color:#f9f9f9;}
	  	#rightPane{
				font-size:10pt;
				color:#000000;
				border-top-style:solid;
				border-width:1px;
				box-shadow:0px 0px 4px 4px #303030; 
				background-color:#f9f9f9;}
      #map{
        margin: 0px;
				padding:0px;
				font-size:8pt;
				font-weight:bold;
				color:#000000;
				border-top-style:solid;
				border-width:1px;
				background-color:#ffffff;}
	  	#header {
				margin: 0px;
				padding:0px;
				font-size:10pt;
				color:#f9f9f9;
				background-color:#424b52;
				box-shadow:0px 0px 4px 4px #303030; 
        text-align:left; vertical-align: middle; font-weight:bold; height:70px;}
	  	h1{
				text-align:center;
				font-size:10pt;
				font-family:Tahoma;
				font-weight:normal;
				color:#494e4c; }
	  	h2{
				text-align:center;
				font-size:10pt;
				font-family:Tahoma;
				color:#494e4c; }
	  	h3{
				text-align:center;
				font-size:12pt;
				font-family:Tahoma;
				font-weight:normal;
				color:#494e4c; }
	  	whitetxt{
				font-size:8pt;
				font-family:Tahoma;
				color:#f9f9f9; }
	  	.esriBasemapGallery {
				font-size:8pt;
				font-family:Tahoma;
				text-align:center;
				color:#494e4c; }
 	  	.esriBasemapGalleryNode {
				float: left;
				margin: 2px 5px 1px 5px;
				width: 150px;
				margin:1px 2px 0 2px;}
	  	.esriBasemapGalleryLabelContainer {
				background-color: transparent;
				text-align: center;
				width: 100%;
				height: 2.5em;
				overflow: hidden;
				display: block; }
	  	.esriBasemapGalleryThumbnail {
				height: 100px;
				width: 150px;
				border: 1px solid #fff;
	 			margin: 1px;
				-moz-box-shadow: 0px 0px 7px #000;
				-webkit-box-shadow: 0px 0px 7px #000;
				box-shadow: 0px 0px 7px #000; }
	  	.esriBasemapGallerySelectedNode .esriBasemapGalleryThumbnail {
				border: 2px solid #F99;
				margin: 0; }
	  	.esriBasemapGalleryMessage {}
    </style>
    <script type="text/javascript"> 
	  <!--
  	if (screen.width <= 800) {
    	window.location = "http://maps.neotreks.com/mobile/";
  	}
  	//-->
		var djConfig = {parseOnLoad: true}; 
		</script>
    <script src="https://js.arcgis.com/3.15/"></script>
    <script type="text/javascript">
      dojo.require("dijit.dijit");
      dojo.require("dijit.layout.BorderContainer");
      dojo.require("dijit.layout.ContentPane");
      dojo.require("esri.map");
      dojo.require("dijit.layout.TabContainer");
			dojo.require("esri.dijit.Scalebar");
			dojo.require("esri.dijit.BasemapGallery");
			dojo.require("esri.layers.FeatureLayer");
      dojo.require("dijit.form.CheckBox");
			dojo.require("dijit.Dialog");
			dojo.require("dijit.TitlePane");
      
	  	//map variables
      var map;
			var loading;
			var poiLayer;
			var mapPOI;
	  	var WEBToken = "?token=<?php echo $tokenNeoTreks; ?>";
			//neotreks maps
			var landuseURL = "http://mapserver.neotreks.com/arcgis/rest/services/NeoTreks/LandUse/MapServer";
			var landcoverURL = "http://mapserver.neotreks.com/arcgis/rest/services/NeoTreks/LandCover/MapServer";
			var slopeURL = "http://mapserver.neotreks.com/arcgis/rest/services/NeoTreks/Slope/MapServer";
			var labelsURL = "http://mapserver.neotreks.com/arcgis/rest/services/NeoTreks/Labels/MapServer";
			var poiURL = "http://mapserver.neotreks.com/ArcGIS/rest/services/NeoTreks/POI/MapServer";
			var poiLayerURL = "http://mapserver.neotreks.com/ArcGIS/rest/services/NeoTreks/POI/MapServer/136";
			//imagery layer
		  var imageryURL = "http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer";
	  
      function init() {	
        var lods = [
				  //{"level": 0,"resolution": 156543.033928,"scale": 591657527.591555},
          //{"level": 1,"resolution": 78271.5169639999,"scale": 295828763.795777},
          //{"level": 2,"resolution": 39135.7584820001,"scale": 147914381.897889},
          {"level": 3,"resolution": 19567.8792409999,"scale": 73957190.948944},
          {"level": 4,"resolution": 9783.93962049996,"scale": 36978595.474472 },
          {"level": 5,"resolution": 4891.96981024998,"scale": 18489297.737236},
          {"level": 6,"resolution": 2445.98490512499,"scale": 9244648.868618},
          {"level": 7,"resolution": 1222.99245256249,"scale": 4622324.434309},
          {"level": 8,"resolution": 611.49622628138,"scale": 2311162.217155},
          {"level": 9,"resolution": 305.748113140558,"scale": 1155581.108577},
          {"level": 10,"resolution": 152.874056570411,"scale": 577790.554289},
          {"level": 11,"resolution": 76.4370282850732,"scale": 288895.277144},
          {"level": 12,"resolution": 38.2185141425366,"scale": 144447.638572},
          {"level": 13,"resolution": 19.1092570712683,"scale": 72223.819286},
          {"level": 14,"resolution": 9.55462853563415,"scale": 36111.909643},
          {"level": 15,"resolution": 4.77731426794937,"scale": 18055.954822},
          {"level": 16,"resolution": 2.38865713397468,"scale": 9027.977411},
          //{"level": 17,"resolution": 1.19432856685505,"scale": 4513.988705},
					//{"level": 18,"resolution": 0.597164283559817,"scale": 2256.994353},
					//{"level": 19,"resolution": 0.298582141647617,"scale": 1128.497176},
        ];
				loading = dojo.byId("loadingImg");
				bar = dojo.byId("bottomBar");
        var initialExtent = new esri.geometry.Extent({"xmin":-19792908,"ymin":136974,"xmax":-1330614,"ymax":8942519,"spatialReference":{"wkid":102100}});
        map = new esri.Map("map", {
          wrapAround180:true,
 		  		extent: initialExtent,
		  		lods: lods
        });
				esri.config.defaults.map.logoLink = "http://www.neotreks.com"    
				document.getElementsByClassName('logo-med')[0].style.backgroundImage="url(\"http://maps.neotreks.com/img/NeoTreksLogo2.png\")";
				document.getElementsByClassName('logo-med')[0].style.backgroundRepeat="no-repeat";
		
				var mapNeoTreksMobile = new esri.layers.ArcGISTiledMapServiceLayer(landuseURL+WEBToken);
				map.addLayer(mapNeoTreksMobile);
				mapPOI = new esri.layers.ArcGISDynamicMapServiceLayer(poiURL+WEBToken);
        map.addLayer(mapPOI);
				map.setExtent(map.extent); 
				dojo.connect(map, "onLoad", loadPOI);
        dojo.connect(map, 'onLoad', function(map) {
		  		var scalebar = new esri.dijit.Scalebar({
          	map: map,
            scalebarUnit: "english"
          }, dojo.byId("scalebar"));
					dojo.connect(dijit.byId('map'), 'resize', resizeMap);
		  		mapPOI.hide();
        });
				dojo.connect(map,"onUpdateStart",showLoading);
        dojo.connect(map,"onUpdateEnd",hideLoading);
				dojo.connect(map, "onExtentChange", function(extent, delta, outLevelChange, outLod) {
					var shortLOD = outLod.scale.toFixed(0)
					var shortLODcomma = addCommas(shortLOD)
          dojo.byId("scale").innerHTML = "1:" + shortLODcomma + "";
        });
				createBasemapGallery();
		}
	  
	  function addCommas(nStr){
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
	  }
	  
	  function loadPOI() {
			var content = "<u>Type</u><br>"+" ${TYPE}<br>" + "<br><u>Category</u><br>"+" ${SUB_TYPE}, ${Class}<br/>";
			var infoTemplate = new esri.InfoTemplate("${NAME}"+"<BR><i><a target='_blank' href='http://www.google.com/search?btnG=Google+Search&q=" + "${NAME}" + "'>  More Info</a></i>", content);
			poiLayer = new esri.layers.FeatureLayer(poiLayerURL+WEBToken, {
				outFields: ["*"],
				infoTemplate: infoTemplate
			});
			map.addLayers([poiLayer]);
			map.infoWindow.resize(200,220);
			poiLayer.visible = false
		}
	  
	  function togglePOI(){
			mapPOI.visible ? mapPOI.hide():mapPOI.show();
			poiLayer.visible ? poiLayer.hide():poiLayer.show();
	  }
	  
	  function createBasemapGallery() {
        var basemapGallery = new esri.dijit.BasemapGallery({
          showArcGISBasemaps: false,
          bingMapsKey: '[bings map key]',
          map: map
        }, "basemapGallery");
		
				var landuse = new esri.dijit.BasemapLayer({url:landuseURL+WEBToken});
				var landcover = new esri.dijit.BasemapLayer({url:landcoverURL+WEBToken});
				var labels = new esri.dijit.BasemapLayer({url:labelsURL+WEBToken});
				var slope = new esri.dijit.BasemapLayer({url:slopeURL+WEBToken});
				var imagery = new esri.dijit.BasemapLayer({url:imageryURL});
		
				var landuseLayer = new esri.dijit.Basemap({
					layers:[landuse],
					visible: true,
					title:"Land Use",
					thumbnailUrl:"img/landuse.jpg"
				});
				var landcoverLayer = new esri.dijit.Basemap({
					layers:[landcover],
					title:"Land Cover",
					thumbnailUrl:"img/landcover.jpg"
				});
				var slopeLayer = new esri.dijit.Basemap({
					layers:[slope],
					title:"Slope",
					thumbnailUrl:"img/slope.jpg"
				});
				var imageryLayer = new esri.dijit.Basemap({
					layers:[imagery,labels],
					title:"Imagery with Labels",
					thumbnailUrl:"img/imagery.jpg" 
				});
		
				basemapGallery.add(landuseLayer);
				basemapGallery.add(landcoverLayer);
				basemapGallery.add(slopeLayer);
				basemapGallery.add(imageryLayer);
		}

		function resizeMap() {
			var resizeTimer;
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function() {
				map.resize();
				map.reposition();
			}, 500);
		}
	  
	  function showLoading() {
			esri.show(loading);
		}

		function hideLoading(error) {
			esri.hide(loading);
		}
	  
		dojo.addOnLoad(init);
    </script>
  </head>
  <body class="claro" >
      <div id="mainWindow" dojotype="dijit.layout.BorderContainer" design="headline" gutters="false" style="width:100%; height:100%;">
      	<div id="map" dojotype="dijit.layout.ContentPane" region="center">
					<div style="position:absolute; right:0px; bottom:0px; z-Index:999;">
						<div dojoType="dijit.layout.ContentPane" style="width:60px; height:60px; overflow:none;">
							<div>
								<img id="loadingImg" src="img/loading.gif" style="width:60px; height:10px; position:absolute; right:10px; bottom:6px; z-index:100;" />
							</div>
						</div>
					</div>
				<div id="scalebar" style="position:absolute;left:10px;bottom:30px;"> </div>
				<span id="scale" style="position:absolute; left:170px; bottom:8px; z-index:100; color:#000000">
			</div>      
      <div id="rightPane"
				data-dojo-type="dijit.layout.ContentPane" 
				data-dojo-props="region:'left'"
				style="width:170px;">
		   	<div data-dojo-type="dijit.layout.BorderContainer" 
					data-dojo-props="design:'headline', gutters:false" 
				 	style="width:100%;height:100%;">
				  <div id="paneHeader" 
					   data-dojo-type="dijit.layout.ContentPane" 
					   data-dojo-props="region:'top'">
						<h2><span>NeoTreks Map Layers</span></h2>
				  </div>
				  <div data-dojo-type="dijit.layout.ContentPane" 
					   data-dojo-props="region:'center'"> 

						<div id="basemapGallery"></div>
						<h2><input type="checkbox" name="mapPOI" onChange="togglePOI();"/>Points of Interest</h2>
				  </div>
				</div>
			</div>
      
	  	<div id="header" 
				data-dojo-type="dijit.layout.ContentPane" 
				data-dojo-props="region:'top'"
		   	style="height:60px;">
				<div dojoType="dijit.layout.BorderContainer" design="headline" gutters="false" >
					<div style="position:absolute; left:25px; top:10px; z-Index:999;">
						<a href="http://www.neotreks.com"><img src="img/NeoTreksLogo.png"></a>
					</div>
					<!--<div style="position:absolute; right:30px; top:23px; z-Index:999;">
						<a href="http://www.neotreks.com"><whitetxt>Neotreks.com</whitetxt></a>
					</div>-->
					<div style="position:absolute; right:30px; top:23px; z-Index:999;">
						<a href="http://neotreks.com/maps/"><whitetxt>About Our Maps</whitetxt></a>
					</div>
					<div style="position:absolute; right:160px; top:23px; z-Index:999;">
						<a href="http://neotreks.com/about/"><whitetxt>About NeoTreks</whitetxt></a>
					</div>
					<div style="position:absolute; right:290px; top:23px; z-Index:999;">
						<a href="http://neotreks.com/maps/faq/"><whitetxt>Embed Our Maps</whitetxt></a>
					</div>
					<div style="position:absolute; right:425px; top:23px; z-Index:999;">
						<a href="http://neotreks.com/maps/legend/"><whitetxt>Map Legend</whitetxt></a>
					</div>
				</div>
			</div>

		</div>
  </body>
</html>